package com.geektechnique.swaggersecurity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomController {

    @RequestMapping(value = "/api/geektechnique", method = RequestMethod.POST)
    public String custom() {
        return "Swagger Test 2";
    }
}
